package com.gitee.easyopen.sdk;

import com.gitee.easyopen.sdk.BaseReq;

/**
 * 没有参数的请求类
 * @author tanghc
 *
 */
public abstract class BaseNoParamReq<T> extends BaseReq<T> {

    public BaseNoParamReq(String name) {
        this(name, "");
    }

    public BaseNoParamReq(String name, String version) {
        this(name, version, null);
    }

    private BaseNoParamReq(String name, String version, Object data) {
        super(name, version, data);
    }

}
