package com.gitee.easyopen.sdk;

import org.junit.Test;

import com.gitee.easyopen.sdk.req.HelloReq;
import com.gitee.easyopen.sdk.resp.HelloResp;

public class HelloTest extends BaseTest {

    @Test
    public void testGet() throws Exception {
        HelloReq req = new HelloReq("hello"); // hello对应@Api中的name属性，即接口名称

        HelloResp result = client.request(req); // 发送请求
        if (result.isSuccess()) {
            String resp = result.getBody();
            System.out.println(resp); // 返回hello world
        } else {
            throw new RuntimeException(result.getMsg());
        }

    }

}