package com.gitee.easyopen.doc.annotation;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Documented
@Retention(RUNTIME)
@Target(METHOD)
public @interface ApiDocMethod {
    /**
     * 描述
     */
    String description() default "";

    ApiDocField[] params() default {};

    Class<?> paramClass() default Object.class;

    ApiDocField[] results() default {};

    Class<?> resultClass() default Object.class;
}
