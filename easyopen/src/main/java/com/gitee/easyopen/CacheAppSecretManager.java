package com.gitee.easyopen;

import java.util.HashMap;
import java.util.Map;

public class CacheAppSecretManager implements AppSecretManager {

    private Map<String, String> secretMap = new HashMap<String, String>(64);

    @Override
    public void addAppSecret(Map<String, String> appSecretStore) {
        secretMap.putAll(appSecretStore);
    }

    @Override
    public String getSecret(String appKey) {
        return secretMap.get(appKey);
    }

    @Override
    public boolean isValidAppKey(String appKey) {
        if (appKey == null) {
            return false;
        }
        return getSecret(appKey) != null;
    }

}
