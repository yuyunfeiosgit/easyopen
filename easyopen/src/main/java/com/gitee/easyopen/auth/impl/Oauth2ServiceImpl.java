package com.gitee.easyopen.auth.impl;

import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.oltu.oauth2.as.issuer.MD5Generator;
import org.apache.oltu.oauth2.as.issuer.OAuthIssuer;
import org.apache.oltu.oauth2.as.issuer.OAuthIssuerImpl;
import org.apache.oltu.oauth2.as.request.OAuthAuthzRequest;
import org.apache.oltu.oauth2.as.request.OAuthTokenRequest;
import org.apache.oltu.oauth2.as.response.OAuthASResponse;
import org.apache.oltu.oauth2.common.OAuth;
import org.apache.oltu.oauth2.common.error.OAuthError;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.message.OAuthResponse;
import org.apache.oltu.oauth2.common.message.types.GrantType;
import org.apache.oltu.oauth2.common.message.types.ResponseType;
import org.apache.oltu.oauth2.common.utils.OAuthUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gitee.easyopen.ApiConfig;
import com.gitee.easyopen.auth.Oauth2Manager;
import com.gitee.easyopen.auth.Oauth2Service;
import com.gitee.easyopen.auth.OpenUser;
import com.gitee.easyopen.exception.LoginErrorException;

/**
 * oauth2服务端默认实现
 * 
 * @author tanghc
 *
 */
public class Oauth2ServiceImpl implements Oauth2Service {
    private static final Logger logger = LoggerFactory.getLogger(Oauth2ServiceImpl.class);

    private Oauth2Manager oauth2Manager;

    public Oauth2ServiceImpl(Oauth2Manager oauth2Manager) {
        super();
        this.oauth2Manager = oauth2Manager;
    }

    @Override
    public OAuthResponse authorize(HttpServletRequest request, HttpServletResponse resp, ApiConfig apiConfig)
            throws URISyntaxException, OAuthSystemException {
        try {
            // 构建OAuth 授权请求
            OAuthAuthzRequest oauthRequest = new OAuthAuthzRequest(request);
            // 检查传入的客户端id是否正确
            if (!checkClientId(oauthRequest.getClientId(), apiConfig)) {
                return OAuthASResponse.errorResponse(HttpServletResponse.SC_BAD_REQUEST)
                        .setError(OAuthError.TokenResponse.INVALID_CLIENT)
                        .setErrorDescription(OAuthError.TokenResponse.INVALID_CLIENT).buildJSONMessage();
            }

            // 如果用户没有登录，跳转到登陆页面
            OpenUser user = null;
            try {
                user = oauth2Manager.login(request);
            } catch (LoginErrorException e) {
                request.setAttribute("error", e.getMessage());
                try {
                    request.getRequestDispatcher(apiConfig.getOauth2LoginUri()).forward(request, resp);
                    throw e;
                } catch (Exception e1) {
                    throw new RuntimeException(e1);
                }
            }

            // 生成授权码
            String authorizationCode = null;
            // responseType目前仅支持CODE，另外还有TOKEN
            String responseType = oauthRequest.getParam(OAuth.OAUTH_RESPONSE_TYPE);
            if (responseType.equals(ResponseType.CODE.toString())) {
                OAuthIssuerImpl oauthIssuerImpl = new OAuthIssuerImpl(new MD5Generator());
                authorizationCode = oauthIssuerImpl.authorizationCode();
                oauth2Manager.addAuthCode(authorizationCode, user);
            }
            // 进行OAuth响应构建
            OAuthASResponse.OAuthAuthorizationResponseBuilder builder = OAuthASResponse.authorizationResponse(request,
                    HttpServletResponse.SC_FOUND);
            // 设置授权码
            builder.setCode(authorizationCode);
            // 得到到客户端重定向地址
            String redirectURI = oauthRequest.getParam(OAuth.OAUTH_REDIRECT_URI);

            // 构建响应
            return builder.location(redirectURI).buildQueryMessage();
        } catch (OAuthProblemException e) {
            // 出错处理
            // logger.error(e.getMessage(),e);
            String redirectUri = e.getRedirectUri();
            if (OAuthUtils.isEmpty(redirectUri)) {
                // 告诉客户端没有传入redirectUri直接报错
                String error = "OAuth redirectUri needs to be provided by client!!!";
                return OAuthASResponse.errorResponse(HttpServletResponse.SC_FOUND)
                        .error(OAuthProblemException.error(error)).location(redirectUri).buildQueryMessage();
            } else {
                // 返回错误消息（如?error=）
                return OAuthASResponse.errorResponse(HttpServletResponse.SC_FOUND).error(e).location(redirectUri)
                        .buildQueryMessage();
            }

        }
    }

    @Override
    public OAuthResponse accessToken(HttpServletRequest request, ApiConfig apiConfig)
            throws URISyntaxException, OAuthSystemException {
        try {
            // 构建OAuth请求
            OAuthTokenRequest oauthRequest = new OAuthTokenRequest(request);

            // 检查提交的客户端id是否正确
            if (!checkClientId(oauthRequest.getClientId(), apiConfig)) {
                return OAuthASResponse.errorResponse(HttpServletResponse.SC_BAD_REQUEST)
                        .setError(OAuthError.TokenResponse.INVALID_CLIENT)
                        .setErrorDescription(OAuthError.TokenResponse.INVALID_CLIENT).buildJSONMessage();
            }

            // 检查客户端安全KEY是否正确
            if (!checkClientSecret(oauthRequest.getClientId(), oauthRequest.getClientSecret(), apiConfig)) {
                return OAuthASResponse.errorResponse(HttpServletResponse.SC_UNAUTHORIZED)
                        .setError(OAuthError.TokenResponse.UNAUTHORIZED_CLIENT)
                        .setErrorDescription(OAuthError.TokenResponse.UNAUTHORIZED_CLIENT).buildJSONMessage();
            }

            String authCode = oauthRequest.getParam(OAuth.OAUTH_CODE);
            // 检查验证类型，此处只检查AUTHORIZATION_CODE类型，其他的还有PASSWORD或REFRESH_TOKEN
            if (oauthRequest.getParam(OAuth.OAUTH_GRANT_TYPE).equals(GrantType.AUTHORIZATION_CODE.toString())) {
                if (!oauth2Manager.checkAuthCode(authCode)) {
                    return OAuthASResponse.errorResponse(HttpServletResponse.SC_BAD_REQUEST)
                            .setError(OAuthError.TokenResponse.INVALID_GRANT).setErrorDescription("错误的授权码")
                            .buildJSONMessage();
                }
            }

            // 生成Access Token
            OAuthIssuer oauthIssuerImpl = new OAuthIssuerImpl(new MD5Generator());
            long expiresIn = oauth2Manager.getExpireIn(apiConfig);
            final String accessToken = oauthIssuerImpl.accessToken();
            OpenUser user = oauth2Manager.getUserByAuthCode(authCode);
            if (user == null) {
                throw OAuthProblemException.error("Can not found user by code.");
            }
            oauth2Manager.addAccessToken(accessToken, user, expiresIn);

            // 生成OAuth响应
            return OAuthASResponse.tokenResponse(HttpServletResponse.SC_OK).setAccessToken(accessToken)
                    .setExpiresIn(String.valueOf(expiresIn)).buildJSONMessage();
        } catch (OAuthProblemException e) {
            logger.error(e.getMessage(), e);
            // 构建错误响应
            return OAuthASResponse.errorResponse(HttpServletResponse.SC_BAD_REQUEST).error(e).buildJSONMessage();
        }
    }

    private boolean checkClientId(String clientId, ApiConfig apiConfig) {
        return apiConfig.getAppSecretManager().isValidAppKey(clientId);
    }

    private boolean checkClientSecret(String clientId, String clientSecret, ApiConfig apiConfig) {
        String secret = apiConfig.getAppSecretManager().getSecret(clientId);
        if (secret == null) {
            return false;
        }
        return secret.equals(clientSecret);
    }

}
