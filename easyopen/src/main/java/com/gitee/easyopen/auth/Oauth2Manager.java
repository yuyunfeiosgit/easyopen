package com.gitee.easyopen.auth;

import javax.servlet.http.HttpServletRequest;

import com.gitee.easyopen.ApiConfig;
import com.gitee.easyopen.exception.LoginErrorException;

/**
 * 认证服务，需要自己实现
 * @author tanghc
 *
 */
public interface Oauth2Manager {

    /**
     * 添加 auth code
     * 
     * @param authCode
     *            code值
     * @param authUser
     *            用户
     */
    void addAuthCode(String authCode, OpenUser authUser);

    /**
     * 添加 access token
     * 
     * @param accessToken
     *            token值
     * @param authUser
     *            用户
     * @param expiresIn 时长,秒
     */
    void addAccessToken(String accessToken, OpenUser authUser, long expiresIn);

    /**
     * 验证auth code是否有效
     * 
     * @param authCode
     * @return 无效返回false
     */
    boolean checkAuthCode(String authCode);

    /**
     * 根据auth code获取用户
     * 
     * @param authCode
     * @return 返回用户
     */
    OpenUser getUserByAuthCode(String authCode);

    /**
     * 根据access token获取用户名
     * 
     * @param accessToken
     *            token值
     * @return 返回用户
     */
    OpenUser getUserByAccessToken(String accessToken);

    /**
     * 获取auth code / access token 过期时间
     * 
     * @return
     */
    long getExpireIn(ApiConfig apiConfig);

    /**
     * 用户登录，需判断是否已经登录
     * @param request
     * @return 返回用户对象
     */
    OpenUser login(HttpServletRequest request) throws LoginErrorException;
}