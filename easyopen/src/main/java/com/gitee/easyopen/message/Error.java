package com.gitee.easyopen.message;

public interface Error<T> {
	String getMsg();
	T getCode();
}
