package com.gitee.easyopen.bean;

import java.lang.reflect.Method;

import com.gitee.easyopen.ApiMeta;

public class ApiDefinition implements ApiMeta {
    private String name;
    private String version;

    private Object handler;
    private Method method;
    private Class<?> methodArguClass; // 方法参数的class

    private boolean ignoreSign;
    private boolean ignoreValidate;
    private boolean wrapResult = Boolean.TRUE;

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @Override
    public Object getHandler() {
        return handler;
    }

    public void setHandler(Object handler) {
        this.handler = handler;
    }

    @Override
    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    @Override
    public Class<?> getMethodArguClass() {
        return methodArguClass;
    }

    public void setMethodArguClass(Class<?> methodArguClass) {
        this.methodArguClass = methodArguClass;
    }

    @Override
    public boolean isIgnoreSign() {
        return ignoreSign;
    }

    public void setIgnoreSign(boolean ignoreSign) {
        this.ignoreSign = ignoreSign;
    }

    @Override
    public boolean isIgnoreValidate() {
        return ignoreValidate;
    }

    public void setIgnoreValidate(boolean ignoreValidate) {
        this.ignoreValidate = ignoreValidate;
    }

    @Override
    public boolean isWrapResult() {
        return wrapResult;
    }

    public void setWrapResult(boolean wrapResult) {
        this.wrapResult = wrapResult;
    }

}
