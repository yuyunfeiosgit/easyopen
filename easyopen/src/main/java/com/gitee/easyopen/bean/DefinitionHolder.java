package com.gitee.easyopen.bean;

import java.util.HashMap;
import java.util.Map;

import com.gitee.easyopen.Param;
import com.gitee.easyopen.exception.DuplicateApiNameException;

public class DefinitionHolder {
    // key:name-version
    private static Map<String, ApiDefinition> apiDefinitionMap = new HashMap<String, ApiDefinition>(64);

    public static void addApiDefinition(ApiDefinition apiDefinition) throws DuplicateApiNameException {
        String key = apiDefinition.getName() + "-" + apiDefinition.getVersion();
        if (apiDefinitionMap.containsKey(key)) {
            throw new DuplicateApiNameException("重复申明接口,name:" + apiDefinition.getName() + " ,version:"
                    + apiDefinition.getVersion() + ",method:" + apiDefinition.getMethod().getName());

        }

        apiDefinitionMap.put(key, apiDefinition);
    }

    public static ApiDefinition getByParam(Param param) {
        String key = param.fatchName() + "-" + param.fatchVersion();
        return apiDefinitionMap.get(key);
    }
}
