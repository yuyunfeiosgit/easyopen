package com.gitee.easyopen.session;

import javax.servlet.http.HttpSession;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.Assert;

/**
 * SessionManager的redis实现，使用redis管理session
 * @author tanghc
 *
 */
public class RedisSessionManager extends ApiSessionManager {

    private RedisTemplate redisTemplate;

    public RedisSessionManager(RedisTemplate redisTemplate) {
        Assert.notNull(redisTemplate, "RedisSessionManager中的redisTemplate不能为null");
        this.redisTemplate = redisTemplate;
    }

    @Override
    public HttpSession getSession(String sessionId) {
        String key = RedisHttpSession.buildKey(sessionId);
        boolean existKey = redisTemplate.hasKey(key);
        if (existKey) {
            return RedisHttpSession.createExistSession(sessionId, getServletContext(), redisTemplate);
        } else {
            return RedisHttpSession.createNewSession(getServletContext(), redisTemplate);
        }
    }

}
